<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */

$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');
$routes->resource('rest-modulo',['controller' => 'RestModulo']);


$routes->get('rest-login/search','RestLogin::search');
$routes->resource('rest-login',['controller' => 'RestLogin']);

$routes->resource('restdepto');
$routes->get('restdepto/actualizar','Restdepto::actualizar');
$routes->get('restdepto/borrar','Restdepto::borrar');
$routes->get('restdepto/publico','Restdepto::publico');

$routes->resource('restcargo');
$routes->get('restcargo/actualizar','Restcargo::actualizar');
$routes->get('restcargo/borrar','Restcargo::borrar');
$routes->get('restcargo/publico','Restcargo::publico');

$routes->resource('restempleado');
$routes->get('restempleado/actualizar','Restempleado::actualizar');
$routes->get('restempleado/borrar','Restempleado::borrar');
$routes->get('restempleado/publico','Restempleado::publico');


$routes->resource('restusuario');
$routes->get('restusuario/actualizar','Restusuario::actualizar');
$routes->get('restusuario/borrar','Restusuario::borrar');

$routes->resource('restcliente');
$routes->get('restcliente/actualizar','Restcliente::actualizar');
$routes->get('restcliente/borrar','Restcliente::borrar');


$routes->resource('resttipopaquete');
$routes->get('resttipopaquete/actualizar','Resttipopaquete::actualizar');
$routes->get('resttipopaquete/borrar','Resttipopaquete::borrar');
$routes->get('resttipopaquete/publico','Resttipopaquete::publico');

$routes->resource('restpaquete');
$routes->get('restpaquete/actualizar','Restpaquete::actualizar');
$routes->get('restpaquete/borrar','Restpaquete::borrar');
$routes->get('restpaquete/publico','Restpaquete::publico');
$routes->get('restpaquete/cliente','Restpaquete::cliente');

$routes->resource('restorden');
$routes->get('restorden/actualizar','Restorden::actualizar');
$routes->get('restorden/borrar','Restorden::borrar');
$routes->get('restorden/publico','Restorden::publico');
$routes->get('restorden/estado','Restorden::estado');
$routes->get('restorden/insertar','Restorden::insertar');


$routes->resource('restservicio');
$routes->get('restservicio/actualizar','Restservicio::actualizar');
$routes->get('restservicio/borrar','Restservicio::borrar');
$routes->get('restservicio/publico','Restservicio::publico');



$routes->resource('restsucursal');
$routes->get('restsucursal/actualizar','Restsucursal::actualizar');
$routes->get('restsucursal/borrar','Restsucursal::borrar');
$routes->get('restsucursal/publico','Restsucursal::publico');



$routes->resource('restfactura');
$routes->get('restfactura/actualizar','Restfactura::actualizar');
$routes->get('restfactura/borrar','Restfactura::borrar');
$routes->get('restfactura/publico','Restfactura::publico');
$routes->get('restfactura/cliente','Restfactura::cliente');

$routes->resource('restdashboard');
$routes->get('restdashboard/informacion_cards','Restdashboard::informacion_cards');
$routes->get('restdashboard/grafica_ordenes','Restdashboard::grafica_ordenes');
/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
