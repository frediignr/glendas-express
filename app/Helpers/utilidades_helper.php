<?php
    

    function ED($fecha){
        $dia = substr($fecha,8,2);
        $mes = substr($fecha,5,2);
        $a = substr($fecha,0,4);
        $fecha = "$dia-$mes-$a";
        return $fecha;
    }
    function MD($fecha){
        $dia = substr($fecha,0,2);
        $mes = substr($fecha,3,2);
        $a = substr($fecha,6,4);
        $fecha = "$a-$mes-$dia";
        return $fecha;
    }

    function restar_meses($fecha, $nmeses)
    {
        $nuevafecha = strtotime ( '-'.$nmeses.' month' , strtotime ( $fecha ) ) ;
        $nuevafecha = date ( 'Y-m-d' , $nuevafecha );
        return $nuevafecha;
    }
    function sumar_meses($fecha, $nmeses)
    {
        $nuevafecha = strtotime ( '+'.$nmeses.' month' , strtotime ( $fecha ) ) ;
        $nuevafecha = date ( 'Y-m-d' , $nuevafecha );
        return $nuevafecha;
    }
    function meses($n)
    {
        $mes = array("ENERO","FEBRERO","MARZO","ABRIL","MAYO","JUNIO","JULIO","AGOSTO","SEPTIEMBRE","OCTUBRE","NOVIEMBRE","DICIEMBRE");
        return $mes[$n-1];
    }
    
    
?>