<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MigracionCrearNuevoCampo extends Migration
{
    public function up()
    {
        $fields = array(
            'columna_de_prueba'
        );
        $this->forge->dropColumn('tblmenu', $fields);
    }

    public function down()
    {
        $this->forge->dropTable('tblmenu');
    }
}
