<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Tblsucursal extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id_sucursal' =>[
                'type'              =>  'INT',
                'constraint'        => 11,
                'unsigned'          => TRUE,
                'auto_increment'    => TRUE
            ],
            'nombre' =>[
                'type'              => 'VARCHAR',
                'constraint'        => '150',
            ],
            'propietario' =>[
                'type'              => 'VARCHAR',
                'constraint'        => '150',
            ],
            'direccion' =>[
                'type'              => 'VARCHAR',
                'constraint'        => '150',
            ],
            'telefono1' =>[
                'type'              => 'VARCHAR',
                'constraint'        => '15',
            ],
            'telefono2' =>[
                'type'              => 'VARCHAR',
                'constraint'        => '15',
                'NULL'              => TRUE
            ],
            'email' =>[
                'type'              => 'VARCHAR',
                'constraint'        => '150',
                'NULL'              =>  TRUE  ,
            ],
            'logo' =>[
                'type'              => 'TEXT',
                'NULL'              =>  TRUE  ,
            ],
            'moneda' =>[
                'type'              => 'VARCHAR',
                'constraint'        => '25',
            ],
            'simbolo' =>[
                'type'              => 'VARCHAR',
                'constraint'        => '3',
            ],
            'id_ciudad' =>[
                'type'              => 'INT',
                'constraint'        => 11,
            ],
            'created_at datetime default current_timestamp',
            'updated_at datetime default current_timestamp',
            'deleted_at datetime default NULL',
        ]);
        $this->forge->addKey('id_sucursal',TRUE);
        $this->forge->createTable('tblsucursal');
    }

    public function down()
    {
        $this->forge->dropTable('tblsucursal');
    }
}
