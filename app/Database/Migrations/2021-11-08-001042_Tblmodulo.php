<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Tblmodulo extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id_modulo' =>[
                'type'              => 'INT',
                'constraint'        => 11,
                'unasigned'         => TRUE,
                'auto_increment'    => TRUE
            ],
            'nombre' =>[
                'type'              => 'VARCHAR',
                'constraint'        => '150',
            ],
            'descripcion'=>[
                'type'              => 'TEXT',
                'NULL'              => TRUE,
            ],
            'filename'=>[
                'type'              => 'VARCHAR',
                'constraint'        => '50'
            ],
            'visible'   =>[
                'type'              => 'TINYINT'
            ],
            'id_menu'   =>[
                'type'              => 'INT',
                'constraint'        =>11
            ],
            'created_at datetime default current_timestamp',
            'updated_at datetime default current_timestamp',
            'deleted_at datetime default NULL',
        ]);
        $this->forge->addKey('id_modulo',TRUE);
        $this->forge->createTable('tblmodulo');
    }

    public function down()
    {
        $this->forge->dropTable('tblmodulo');
    }
}
