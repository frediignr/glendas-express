<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Tblmenu extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id_menu' =>[
                'type'              =>  'INT',
                'constraint'        => 11,
                'unsigned'          => TRUE,
                'auto_increment'    => TRUE
            ],
            'nombre' =>[
                'type'              => 'VARCHAR',
                'constraint'        => '100',
            ],
            'prioridad' =>[
                'type'              => 'INT',
                'constraint'        => 11,
            ],
            'icono' =>[
                'type'              => 'VARCHAR',
                'constraint'        => '100',
            ],
            'visible' =>[
                'type'              => 'TINYINT',
            ],
            'super_admin' =>[
                'type'              => 'TINYINT',
            ],
            'created_at datetime default current_timestamp',
            'updated_at datetime default current_timestamp',
            'deleted_at datetime default NULL',
        ]);
        $this->forge->addKey('id_menu',TRUE);
        $this->forge->createTable('tblmenu');
        $fields = array(
            'super_admin' => array(
                    'type' => 'TINYINT',
            )
        );
        $this->dbforge->add_column('tblmenu', $fields);
    }

    public function down()
    {
        $this->forge->dropTable('tblmenu');
    }
}
