<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Tblcargo extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id_cargo'  =>[
                'type'              =>  'INT',
                'constraint'            =>  11,
                'unasigned'             =>  TRUE,
                'auto_increment'        =>  TRUE
            ],
            'nombre' =>[
                'type'              => 'VARCHAR',
                'constraint'        => '100',
            ],
            'descripcion' =>[
                'type'              =>  'TEXT',
            ],
            'id_depto'  =>[
                'type'              =>  'INT',
                'constraint'        =>  11
            ],
            'created_at datetime default current_timestamp',
            'updated_at datetime default current_timestamp',
            'deleted_at datetime default NULL',
        ]);
        $this->forge->addKey('id_cargo',TRUE);
        $this->forge->createTable('tblcargo');
    }

    public function down()
    {
        $this->forge->dropTable('tblcargo');
    }
}
