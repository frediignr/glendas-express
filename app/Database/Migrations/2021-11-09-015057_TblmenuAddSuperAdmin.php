<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class TblmenuAddSuperAdmin extends Migration
{
    public function up()
    {
        $fields = array(
            'super_admin' => array(
                    'type' => 'TINYINT',
            )
        );
        $this->forge->addColumn('tblmenu', $fields);
    }

    public function down()
    {
        $this->forge->dropTable('tblmenu');
    }
}
