<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MigracionNuevoCampoMenu extends Migration
{
    public function up()
    {
        $fields = array(
            'columna_de_prueba' => array(
                    'type' => 'TINYINT',
            )
        );
        $this->forge->addColumn('tblmenu', $fields);
    }

    public function down()
    {
        $this->forge->dropTable('tblmenu');
    }
}
