<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Tblciudad extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id_ciudad' =>[
                'type'              =>  'INT',
                'constraint'        => 11,
                'unsigned'          => TRUE,
                'auto_increment'    => TRUE
            ],
            'nombre' =>[
                'type'              => 'VARCHAR',
                'constraint'        => '100',
            ],
            'id_pais' =>[
                'type'              => 'INT',
                'constraint'        => 11,
            ],
            'created_at datetime default current_timestamp',
            'updated_at datetime default current_timestamp',
            'deleted_at datetime default NULL',
        ]);
        $this->forge->addKey('id_ciudad',TRUE);
        $this->forge->createTable('tblciudad');
    
    }

    public function down()
    {
        $this->forge->dropTable('tblciudad');
    }
}
