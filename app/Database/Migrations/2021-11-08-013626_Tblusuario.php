<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Tblusuario extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id_usuario' =>[
                'type'              =>  'INT',
                'constraint'        => 11,
                'unsigned'          => TRUE,
                'auto_increment'    => TRUE
            ],
            'nombre' =>[
                'type'              => 'VARCHAR',
                'constraint'        => '100',
            ],
            'usuario' =>[
                'type'              => 'VARCHAR',
                'constraint'        => '50',
            ],
            'password' =>[
                'type'              => 'TEXT',
            ],
            'imagen' =>[
                'type'              => 'TEXT',
            ],
            'admin' =>[
                'type'              => 'TINYINT',
                'constraint'        => '1',
            ],
            'id_empleado' =>[
                'type'              => 'INT',
                'constraint'        => 11,
            ],
            'id_sucursal' =>[
                'type'              => 'INT',
                'constraint'        => 11,
            ],
            'created_at datetime default current_timestamp',
            'updated_at datetime default current_timestamp',
            'deleted_at datetime default NULL',
        ]);
        $this->forge->addKey('id_usuario',TRUE);
        $this->forge->createTable('tblusuario');
    }

    public function down()
    {
        $this->forge->dropTable('tblusuario');
    }
}
