<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Tblpais extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id_pais' =>[
                'type'              =>  'INT',
                'constraint'        => 11,
                'unsigned'          => TRUE,
                'auto_increment'    => TRUE
            ],
            'nombre' =>[
                'type'              => 'VARCHAR',
                'constraint'        => '100',
            ],
            'created_at datetime default current_timestamp',
            'updated_at datetime default current_timestamp',
            'deleted_at datetime default NULL',
        ]);
        $this->forge->addKey('id_pais',TRUE);
        $this->forge->createTable('tblpais');
    }

    public function down()
    {
        $this->forge->dropTable('tblpais');
    }
}
