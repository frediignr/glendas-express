<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class TblusuarioModulo extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id_usuario_modulo' =>[
                'type'              =>  'INT',
                'constraint'        => 11,
                'unsigned'          => TRUE,
                'auto_increment'    => TRUE
            ],
            'id_modulo' =>[
                'type'              => 'INT',
                'constraint'        => 11,
            ],
            'id_usuario' =>[
                'type'              => 'INT',
                'constraint'        => 11,
            ],
            'created_at datetime default current_timestamp',
            'updated_at datetime default current_timestamp',
            'deleted_at datetime default NULL',
        ]);
        $this->forge->addKey('id_usuario_modulo',TRUE);
        $this->forge->createTable('tblusuario_modulo');
    }

    public function down()
    {
        $this->forge->dropTable('tblusuario_modulo');
    }
}
