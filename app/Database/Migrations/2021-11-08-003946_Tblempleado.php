<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Tblempleado extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id_empleado'   =>[
                'type'              =>  'INT',
                'constraint'        =>  11,
                'unsigned'          =>  TRUE,
                'auto_increment'    =>  TRUE,
            ],
            'nombres' =>[
                'type'              => 'VARCHAR',
                'constraint'        => '100',
            ],
            'apellidos' =>[
                'type'              => 'VARCHAR',
                'constraint'        => '100',
            ],
            'direccion' =>[
                'type'              => 'VARCHAR',
                'constraint'        => '150',
            ],
            'telefono' =>[
                'type'              => 'VARCHAR',
                'constraint'        => '15',
            ],
            'fecha_nacimiento' =>[
                'type'              => 'DATE',
            ],
            'fecha_inicio' =>[
                'type'              => 'DATE',
            ],
            'id_cargo'  =>[
                'type'              => 'INT',
                'constraint'        => 11,
            ],
            'id_sucursal'  =>[
                'type'              => 'INT',
                'constraint'        => 11,
            ],
            'created_at datetime default current_timestamp',
            'updated_at datetime default current_timestamp',
            'deleted_at datetime default NULL'
        ]);
        $this->forge->addKey('id_empleado',TRUE);
        $this->forge->createTable('tblempleado');
    }

    public function down()
    {
        $this->forge->dropTable('tblempleado');
    }
}
