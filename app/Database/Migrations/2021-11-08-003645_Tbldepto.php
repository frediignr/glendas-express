<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Tbldepto extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id_depto'  =>[
                'type'              =>  'INT',
                'constraint'        =>  11,
                'unsigned'         =>  TRUE,
                'auto_increment'    =>  TRUE
            ],
            'nombre' =>[
                'type'              => 'VARCHAR',
                'constraint'        => '100',
            ],
            'descripcion'   =>[
                'type'              => 'TEXT'
            ],
            'created_at datetime default current_timestamp',
            'updated_at datetime default current_timestamp',
            'deleted_at datetime default NULL',
        ]);
        $this->forge->addKey('id_depto',TRUE);
        $this->forge->createTable('tbldepto');
    }

    public function down()
    {
        $this->forge->dropTable('tbldepto');
    }
}
