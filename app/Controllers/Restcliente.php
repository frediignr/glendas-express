<?php namespace App\Controllers;


use App\Models\ClienteModel;
use MyRestApi;
include_once (dirname(__FILE__) . "/MyRestApi.php");
// 1. Para las solicitudes GET / POST / PUT ordinarias, el encabezado de la solicitud se establece de la siguiente manera:
// Establecer el encabezado de solicitud de formato json
header("Content-type:application/json; charset=utf-8");
// La solicitud entre dominios permite la configuración del nombre de dominio, porque las cookies deben pasarse, no se pueden usar *
header("Access-Control-Allow-Origin: *");
// Solicitud de encabezados permitidos para solicitudes entre dominios
header("Access-Control-Allow-Headers: Content-type");
// Solicitud de consentimiento entre dominios para enviar cookies
header("Access-Control-Allow-Credentials: true");
 
// 2. Solicitud no simple Antes de cada solicitud, se enviará una solicitud de "verificación previa", que es el método de solicitud de opciones. Es principalmente para preguntarle al servidor si permite el acceso de esta solicitud no simple. Si lo permitimos, entonces devolvemos el encabezado de respuesta requerido. El encabezado de solicitud de esta solicitud de verificación previa se establece de la siguiente manera:
// Establecer el encabezado de solicitud de formato json
header("Content-type:application/json; charset=utf-8");
// Configuración de nombre de dominio permitida para solicitud entre dominios
header("Access-Control-Allow-Origin: *");
// Solicitud de encabezados permitidos para solicitudes entre dominios
header("Access-Control-Allow-Headers: Content-type");
header("Vary: Accept-Encoding, Origin");
// Solicitud de consentimiento entre dominios para enviar cookies
header("Access-Control-Allow-Credentials: true");
// métodos permitidos en la solicitud de opciones
header("Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS");
// OPCIONES este tiempo de validez de la solicitud previa, 20 días
header("Access-Control-Max-Age: 1728000");
class Restcliente extends MyRestApi
{

    protected $modelName = 'App\Models\ClienteModel';
    protected $format = 'json';
    public function index(){
        $cliente = new ClienteModel();
        return $this->genericResponse($cliente->get(),null,200);
    }

    public function create()
    {
        $cliente = new ClienteModel();
        $nombres = addslashes($this->request->getPost('nombres'));
        $apellidos = addslashes($this->request->getPost('apellidos'));     
        $direccion = addslashes($this->request->getPost('direccion'));
        $telefono = addslashes($this->request->getPost('telefono'));    
        $dui = addslashes($this->request->getPost('dui'));   
        if($nombres == "" ||$apellidos == "" ||$direccion == "" ||$telefono == "" ||$dui == "" ){
            return $this->genericResponse(null,"Error, tiene que llenar todos los campos.!",500);
        }
        $id = $cliente->insert([
            'nombres' => $nombres,
            'apellidos' => $apellidos,
            'direccion' => $direccion,
            'telefono' => $telefono,
            'dui' => $dui,
        ]);
        return $this->genericResponse($this->model->find($id), null, 200);
    }

    public function show($id = null)
    {
        return $this->genericResponse($this->model->find($id),null,200);
    }
    public function actualizar(){
        $cliente = new ClienteModel();
        $id = addslashes($this->request->getPost('id_cliente'));
        $nombres = addslashes($this->request->getPost('nombres'));
        $apellidos = addslashes($this->request->getPost('apellidos'));     
        $direccion = addslashes($this->request->getPost('direccion'));
        $telefono = addslashes($this->request->getPost('telefono'));   
        $dui = addslashes($this->request->getPost('dui'));       
        if($nombres == "" ||$apellidos == "" ||$direccion == "" ||$telefono == "" ||$dui == "" ){
            return $this->genericResponse(null,"Error, tiene que llenar todos los campos.!",500);
        }
        if (!$cliente->get($id)) {
            return $this->genericResponse(null, array("id_cliente" => "No Existe el Cliente"), 500);
        }
        $cliente->update($id,[
            'nombres' => $nombres,
            'apellidos' => $apellidos,
            'direccion' => $direccion,
            'telefono' => $telefono,
            'dui' => $dui,
        ]);
        return $this->genericResponse($this->model->find($id), null, 200);
    }
    //LA FUNCION PARA VER UN DEPARTAMENTO EN ESPECIFICO YA SE ENCUENTRA REALIZADA Y NO HAY QUE CREAR OTRA

    public function borrar(){
        $cliente = new ClienteModel();
        $id = addslashes($this->request->getPost('id_cliente'));
        $cliente->delete($id);
        return $this->genericResponse("Cliente eliminado",null,200);
    }

    public function publico(){
        $cliente = new ClienteModel();
        $array = $cliente->get();
        $nuevo_array = array();
        $count = 0;
        foreach ($array as $key => $value) {
            $nuevo_array[] = array(
                'text' => $value['nombres']." ".$value['apellidos'],
                'value' => $value['id_cliente']
            );
            $count++;
        }
        return $this->genericResponse($nuevo_array,null,200);
    }
}