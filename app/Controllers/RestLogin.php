<?php namespace App\Controllers;

use MyRestApi;
include_once (dirname(__FILE__) . "/MyRestApi.php");
header('Access-Control-Allow-Origin: *');
class RestLogin extends MyRestApi
{
    protected $modelName = 'App\Models\LoginModel';
    protected $format = 'json';
    public function search(){
        $usuario = addslashes($this->request->getGet('username'));
        $password = addslashes($this->request->getGet('password'));
        $password = md5($password);
        if (!$usuario || !$password) {
            return $this->genericResponse(null,"Error, tiene que llenar todos los campos.!",500);
        }
        $usuario_existe = $this->model
        ->select('tblusuario.id_usuario, tblusuario.nombre, tblusuario.imagen,tblusuario.admin,tblusuario.id_empleado,tblusuario.id_sucursal,tblusuario.super_admin,tblsucursal.nombre as nombre_sucursal')
        ->join('tblsucursal','tblusuario.id_sucursal = tblsucursal.id_sucursal')
        ->where('usuario',$usuario)
        ->where('password',$password)
        ->findAll();
        if(!$usuario_existe){
            return $this->genericResponse(null,"Error, No existe ese usuario.!",500);
        }
        $array_devolver = array();
        foreach ($usuario_existe as $key => $value) {
            $id_usuario = $this->encriptar($value['id_usuario']);
            $nombre = $value['nombre'];
            $imagen = $value['imagen'];
            $admin =  $this->encriptar($value['admin']);
            $id_empleado = $this->encriptar($value['id_empleado']);
            $id_sucursal = $this->encriptar($value['id_sucursal']);
            $super_admin = $this->encriptar($value['super_admin']);
            $array_devolver['id_usuario'] = $id_usuario;
            $array_devolver['nombre'] = $nombre;
            $array_devolver['imagen'] = $imagen;
            $array_devolver['admin'] = $admin;
            $array_devolver['id_empleado'] = $id_empleado;
            $array_devolver['id_sucursal'] = $id_sucursal;
            $array_devolver['super_admin'] = $super_admin;
            $array_devolver['nombre_sucursal'] = $value['nombre_sucursal'];

        }
        return $this->genericResponse($array_devolver,null,200);

    }
    
}