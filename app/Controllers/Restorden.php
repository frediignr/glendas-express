<?php namespace App\Controllers;


use App\Models\OrdenModel;
use MyRestApi;
include_once (dirname(__FILE__) . "/MyRestApi.php");
// 1. Para las solicitudes GET / POST / PUT ordinarias, el encabezado de la solicitud se establece de la siguiente manera:
// Establecer el encabezado de solicitud de formato json
header("Content-type:application/json; charset=utf-8");
// La solicitud entre dominios permite la configuración del nombre de dominio, porque las cookies deben pasarse, no se pueden usar *
header("Access-Control-Allow-Origin: *");
// Solicitud de encabezados permitidos para solicitudes entre dominios
header("Access-Control-Allow-Headers: Content-type");
// Solicitud de consentimiento entre dominios para enviar cookies
header("Access-Control-Allow-Credentials: true");
 
// 2. Solicitud no simple Antes de cada solicitud, se enviará una solicitud de "verificación previa", que es el método de solicitud de opciones. Es principalmente para preguntarle al servidor si permite el acceso de esta solicitud no simple. Si lo permitimos, entonces devolvemos el encabezado de respuesta requerido. El encabezado de solicitud de esta solicitud de verificación previa se establece de la siguiente manera:
// Establecer el encabezado de solicitud de formato json
header("Content-type:application/json; charset=utf-8");
// Configuración de nombre de dominio permitida para solicitud entre dominios
header("Access-Control-Allow-Origin: *");
// Solicitud de encabezados permitidos para solicitudes entre dominios
header("Access-Control-Allow-Headers: Content-type");
header("Vary: Accept-Encoding, Origin");
// Solicitud de consentimiento entre dominios para enviar cookies
header("Access-Control-Allow-Credentials: true");
// métodos permitidos en la solicitud de opciones
header("Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS");
// OPCIONES este tiempo de validez de la solicitud previa, 20 días
header("Access-Control-Max-Age: 1728000");
class Restorden extends MyRestApi
{

    protected $modelName = 'App\Models\OrdenModel';
    protected $format = 'json';
    public function index(){
        $orden = new OrdenModel();
        return $this->genericResponse($orden->get(),null,200);
    }

    public function insertar()
    {
        $orden = new OrdenModel();
        $id_cliente = addslashes($this->request->getPost('id_cliente'));        
        $id_origen = addslashes($this->request->getPost('id_origen'));   
        $id_destino = addslashes($this->request->getPost('id_destino'));
        $correo = addslashes($this->request->getPost('correo'));
        $documento_recibe = addslashes($this->request->getPost('documento_recibe'));
        $nombre_recibe = addslashes($this->request->getPost('nombre_recibe'));
        $detalles = addslashes($this->request->getPost('detalles'));

        if($id_cliente == "" || $id_origen == "" || $id_destino == "" || $correo == "" || $documento_recibe == "" || $nombre_recibe == ""){
            return $this->genericResponse(null,"Error, tiene que llenar todos los campos.!",500);
        }
        $numero_seguimiento = $this->generar_codigo();
        $id = $orden->insert([
            'numero_seguimiento' => $numero_seguimiento,
            'id_cliente' => $id_cliente,
            'id_envio' => $id_origen,
            'id_recibo' => $id_destino,
            'fecha' => date("Y-m-d"),
            'hora' => date("H:i:s"),
            'detalles' =>  $detalles,
            'correo' => $correo,
            'nombre_recibe' => $nombre_recibe,
            'dui_recibe' => $documento_recibe,
            'id_estado' => '1'
        ]);
        $orden->actualizar_paquetes($id_cliente,$id);
        $to = $this->request->getPost('correo');
        $subject = "Informacion de la orden";
        $headers = "From: soporte@fashionmen.web-uis.com". "\r\n";
        $headers .= "CC:  soporte@fashionmen.web-uis.com";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        $message = "
        <html>
        <head>
        <title>Gracias por hacer sus envios con nosotros!</title>
        </head>
        <body>
        <h1>Este es el numero de seguimiento para tu orden: </h1>
        <p>$numero_seguimiento</p>

        </body>
        </html>";
        
        mail($to, $subject, $message,$headers);


        return $this->genericResponse($this->model->find($id), null, 200);
    }

    public function show($id = null)
    {
        return $this->genericResponse($this->model->find($id),null,200);
    }

    public function estado($id = null)
    { 
        $orden = new OrdenModel();
        return $this->genericResponse($orden->estado($id),null,200);
    }

    public function actualizar(){
        $orden = new OrdenModel();
        $id = addslashes($this->request->getPost('id_orden'));
        $id_estado = addslashes($this->request->getPost('id_estado'));
        if($id_estado == ""){
            return $this->genericResponse(null,"Error, tiene que llenar todos los campos.!",500);
        }
        if (!$orden->get($id)) {
            return $this->genericResponse(null, array("id_orden" => "No Existe la Orden"), 500);
        }
        $orden->update($id,[
            'id_estado' => $id_estado,
        ]);
        return $this->genericResponse($this->model->find($id), null, 200);
    }
    //LA FUNCION PARA VER UN DEPARTAMENTO EN ESPECIFICO YA SE ENCUENTRA REALIZADA Y NO HAY QUE CREAR OTRA

    public function borrar(){
        $orden = new OrdenModel();
        $id = addslashes($this->request->getPost('id_orden'));
        $orden->delete($id);
        $orden->recuperar_paquetes($id);
        return $this->genericResponse("Orden eliminada",null,200);
    }
    public function publico(){
        $orden = new OrdenModel();
        $array = $orden->get();
        $nuevo_array = array();
        $count = 0;
        foreach ($array as $key => $value) {
            $nuevo_array[] = array(
                'text' => $value['numero_seguimiento'],
                'value' => $value['id_orden']
            );
            $count++;
        }
        return $this->genericResponse($nuevo_array,null,200);
    }
    public function generar_codigo(){
        $codigo = "";
        $permitted_chars1 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $permitted_chars2 = '0123456789';
        $codigo1 = substr(str_shuffle($permitted_chars1), 0, 5);
        $codigo2 = substr(str_shuffle($permitted_chars2), 0, 5);
        $codigo = $codigo1.$codigo2;
        return $codigo;
    }
}