<?php namespace App\Controllers;


use App\Models\CargoModel;
use MyRestApi;
include_once (dirname(__FILE__) . "/MyRestApi.php");
// 1. Para las solicitudes GET / POST / PUT ordinarias, el encabezado de la solicitud se establece de la siguiente manera:
// Establecer el encabezado de solicitud de formato json
header("Content-type:application/json; charset=utf-8");
// La solicitud entre dominios permite la configuración del nombre de dominio, porque las cookies deben pasarse, no se pueden usar *
header("Access-Control-Allow-Origin: *");
// Solicitud de encabezados permitidos para solicitudes entre dominios
header("Access-Control-Allow-Headers: Content-type");
// Solicitud de consentimiento entre dominios para enviar cookies
header("Access-Control-Allow-Credentials: true");
 
// 2. Solicitud no simple Antes de cada solicitud, se enviará una solicitud de "verificación previa", que es el método de solicitud de opciones. Es principalmente para preguntarle al servidor si permite el acceso de esta solicitud no simple. Si lo permitimos, entonces devolvemos el encabezado de respuesta requerido. El encabezado de solicitud de esta solicitud de verificación previa se establece de la siguiente manera:
// Establecer el encabezado de solicitud de formato json
header("Content-type:application/json; charset=utf-8");
// Configuración de nombre de dominio permitida para solicitud entre dominios
header("Access-Control-Allow-Origin: *");
// Solicitud de encabezados permitidos para solicitudes entre dominios
header("Access-Control-Allow-Headers: Content-type");
header("Vary: Accept-Encoding, Origin");
// Solicitud de consentimiento entre dominios para enviar cookies
header("Access-Control-Allow-Credentials: true");
// métodos permitidos en la solicitud de opciones
header("Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS");
// OPCIONES este tiempo de validez de la solicitud previa, 20 días
header("Access-Control-Max-Age: 1728000");
class Restcargo extends MyRestApi
{

    protected $modelName = 'App\Models\CargoModel';
    protected $format = 'json';
    public function index(){
        $cargo = new CargoModel();
        return $this->genericResponse($cargo->get(),null,200);
    }

    public function create()
    {
        $cargo = new CargoModel();
        $nombre = addslashes($this->request->getPost('nombre'));
        $descripcion = addslashes($this->request->getPost('descripcion'));        
        $id_depto = addslashes($this->request->getPost('id_depto'));   
        if($nombre == "" || $id_depto == ""){
            return $this->genericResponse(null,"Error, tiene que llenar todos los campos.!",500);
        }
        $id = $cargo->insert([
            'descripcion' => $descripcion,
            'nombre' => $nombre,
            'id_depto' =>$id_depto
        ]);
        return $this->genericResponse($this->model->find($id), null, 200);
    }

    public function show($id = null)
    {
        return $this->genericResponse($this->model->find($id),null,200);
    }
    public function actualizar(){
        $cargo = new CargoModel();
        $id = addslashes($this->request->getPost('id_cargo'));
        $id_depto = addslashes($this->request->getPost('id_depto'));        
        $nombre = addslashes($this->request->getPost('nombre'));
        $descripcion = addslashes($this->request->getPost('descripcion'));         
        if($nombre == ""){
            return $this->genericResponse(null,"Error, tiene que llenar todos los campos.!",500);
        }
        if (!$cargo->get($id)) {
            return $this->genericResponse(null, array("id_cargo" => "No Existe el Cargo"), 500);
        }
        $cargo->update($id,[
            'nombre' => $nombre,
            'descripcion' => $descripcion,
            'id_depto' => $id_depto
        ]);
        return $this->genericResponse($this->model->find($id), null, 200);
    }
    //LA FUNCION PARA VER UN DEPARTAMENTO EN ESPECIFICO YA SE ENCUENTRA REALIZADA Y NO HAY QUE CREAR OTRA

    public function borrar(){
        $cargo = new CargoModel();
        $id = addslashes($this->request->getPost('id_cargo'));
        $cargo->delete($id);
        return $this->genericResponse("Cargo eliminado",null,200);
    }
    public function publico(){
        $cargo = new CargoModel();
        $array = $cargo->get();
        $nuevo_array = array();
        $count = 0;
        foreach ($array as $key => $value) {
            $nuevo_array[] = array(
                'text' => $value['nombre'],
                'value' => $value['id_cargo']
            );
            $count++;
        }
        return $this->genericResponse($nuevo_array,null,200);
    }
}