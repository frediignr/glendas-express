<?php namespace App\Controllers;


use App\Models\PaqueteModel;
use MyRestApi;
include_once (dirname(__FILE__) . "/MyRestApi.php");
// 1. Para las solicitudes GET / POST / PUT ordinarias, el encabezado de la solicitud se establece de la siguiente manera:
// Establecer el encabezado de solicitud de formato json
header("Content-type:application/json; charset=utf-8");
// La solicitud entre dominios permite la configuración del nombre de dominio, porque las cookies deben pasarse, no se pueden usar *
header("Access-Control-Allow-Origin: *");
// Solicitud de encabezados permitidos para solicitudes entre dominios
header("Access-Control-Allow-Headers: Content-type");
// Solicitud de consentimiento entre dominios para enviar cookies
header("Access-Control-Allow-Credentials: true");
 
// 2. Solicitud no simple Antes de cada solicitud, se enviará una solicitud de "verificación previa", que es el método de solicitud de opciones. Es principalmente para preguntarle al servidor si permite el acceso de esta solicitud no simple. Si lo permitimos, entonces devolvemos el encabezado de respuesta requerido. El encabezado de solicitud de esta solicitud de verificación previa se establece de la siguiente manera:
// Establecer el encabezado de solicitud de formato json
header("Content-type:application/json; charset=utf-8");
// Configuración de nombre de dominio permitida para solicitud entre dominios
header("Access-Control-Allow-Origin: *");
// Solicitud de encabezados permitidos para solicitudes entre dominios
header("Access-Control-Allow-Headers: Content-type");
header("Vary: Accept-Encoding, Origin");
// Solicitud de consentimiento entre dominios para enviar cookies
header("Access-Control-Allow-Credentials: true");
// métodos permitidos en la solicitud de opciones
header("Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS");
// OPCIONES este tiempo de validez de la solicitud previa, 20 días
header("Access-Control-Max-Age: 1728000");
class Restpaquete extends MyRestApi
{

    protected $modelName = 'App\Models\PaqueteModel';
    protected $format = 'json';
    public function index(){
        $cargo = new PaqueteModel();
        return $this->genericResponse($cargo->get(),null,200);
    }

    public function create()
    {
        $cargo = new PaqueteModel();
        $descripcion = addslashes($this->request->getPost('descripcion'));        
        $peso = addslashes($this->request->getPost('peso'));   
        $id_tipo_paquete = addslashes($this->request->getPost('id_tipo_paquete'));
        $id_cliente = addslashes($this->request->getPost('id_cliente'));
        $precio = addslashes($this->request->getPost('precio'));
        $lugar_compra = addslashes($this->request->getPost('lugar_compra'));

        if($descripcion == "" || $peso == "" || $id_tipo_paquete == "" || $id_cliente == "" || $precio == "" || $lugar_compra == ""){
            return $this->genericResponse(null,"Error, tiene que llenar todos los campos.!",500);
        }
        $id = $cargo->insert([
            'descripcion' => $descripcion,
            'fecha' => date("Y-m-d"),
            'hora' => date("H:i:s"),
            'peso' => $peso,
            'id_cliente' => $id_cliente,
            'id_tipo_paquete' =>  $id_tipo_paquete,
            'precio' => $precio,
            'lugar_compra' => $lugar_compra
        ]);
        return $this->genericResponse($this->model->find($id), null, 200);
    }

    public function show($id = null)
    {
        return $this->genericResponse($this->model->find($id),null,200);
    }
    public function orden($id = null)
    { 
        $paquete = new PaqueteModel();
        return $this->genericResponse($paquete->orden($id),null,200);
    }
    public function cliente($id = null)
    { 
        $paquete = new PaqueteModel();
        return $this->genericResponse($paquete->cliente($id),null,200);
    }
    public function actualizar(){
        $cargo = new PaqueteModel();
        $id = addslashes($this->request->getPost('id_paquete'));
        $descripcion = addslashes($this->request->getPost('descripcion'));        
        $peso = addslashes($this->request->getPost('peso'));   
        $id_tipo_paquete = addslashes($this->request->getPost('id_tipo_paquete'));
        $id_cliente = addslashes($this->request->getPost('id_cliente'));     
        $precio = addslashes($this->request->getPost('precio'));
        $lugar_compra = addslashes($this->request->getPost('lugar_compra'));  
        if($descripcion == "" || $peso == "" || $id_tipo_paquete == "" || $id_cliente == "" || $precio == "" || $lugar_compra == ""){
            return $this->genericResponse(null,"Error, tiene que llenar todos los campos.!",500);
        }
        if (!$cargo->get($id)) {
            return $this->genericResponse(null, array("id_paquete" => "No Existe el Paquete"), 500);
        }
        $cargo->update($id,[
            'descripcion' => $descripcion,
            'peso' => $peso,
            'id_cliente' => $id_cliente,
            'id_tipo_paquete' =>  $id_tipo_paquete,
            'precio' => $precio,
            'lugar_compra' => $lugar_compra
        ]);
        return $this->genericResponse($this->model->find($id), null, 200);
    }
    //LA FUNCION PARA VER UN DEPARTAMENTO EN ESPECIFICO YA SE ENCUENTRA REALIZADA Y NO HAY QUE CREAR OTRA

    public function borrar(){
        $cargo = new PaqueteModel();
        $id = addslashes($this->request->getPost('id_paquete'));
        $cargo->delete($id);
        return $this->genericResponse("Paquete eliminado",null,200);
    }
    public function publico(){
        $cargo = new PaqueteModel();
        $array = $cargo->get();
        $nuevo_array = array();
        $count = 0;
        foreach ($array as $key => $value) {
            $nuevo_array[] = array(
                'text' => $value['nombre'],
                'value' => $value['id_cargo']
            );
            $count++;
        }
        return $this->genericResponse($nuevo_array,null,200);
    }
}