<?php namespace App\Controllers;


use App\Models\DashboardModel;
use MyRestApi;
include_once (dirname(__FILE__) . "/MyRestApi.php");
// 1. Para las solicitudes GET / POST / PUT ordinarias, el encabezado de la solicitud se establece de la siguiente manera:
// Establecer el encabezado de solicitud de formato json
header("Content-type:application/json; charset=utf-8");
// La solicitud entre dominios permite la configuración del nombre de dominio, porque las cookies deben pasarse, no se pueden usar *
header("Access-Control-Allow-Origin: *");
// Solicitud de encabezados permitidos para solicitudes entre dominios
header("Access-Control-Allow-Headers: Content-type");
// Solicitud de consentimiento entre dominios para enviar cookies
header("Access-Control-Allow-Credentials: true");
 
// 2. Solicitud no simple Antes de cada solicitud, se enviará una solicitud de "verificación previa", que es el método de solicitud de opciones. Es principalmente para preguntarle al servidor si permite el acceso de esta solicitud no simple. Si lo permitimos, entonces devolvemos el encabezado de respuesta requerido. El encabezado de solicitud de esta solicitud de verificación previa se establece de la siguiente manera:
// Establecer el encabezado de solicitud de formato json
header("Content-type:application/json; charset=utf-8");
// Configuración de nombre de dominio permitida para solicitud entre dominios
header("Access-Control-Allow-Origin: *");
// Solicitud de encabezados permitidos para solicitudes entre dominios
header("Access-Control-Allow-Headers: Content-type");
header("Vary: Accept-Encoding, Origin");
// Solicitud de consentimiento entre dominios para enviar cookies
header("Access-Control-Allow-Credentials: true");
// métodos permitidos en la solicitud de opciones
header("Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS");
// OPCIONES este tiempo de validez de la solicitud previa, 20 días
header("Access-Control-Max-Age: 1728000");
class Restdashboard extends MyRestApi
{

    protected $modelName = 'App\Models\DashboardModel';
    protected $format = 'json';
    public function index(){
        $dashboard = new DashboardModel();
        return $this->genericResponse($dashboard->get(),null,200);
    }
    public function informacion_cards(){
        $dashboard = new DashboardModel();
        $array =  $dashboard->informacion_cards()->getResultArray();
        return $this->genericResponse($array,null,200);
    }
    public function grafica_ordenes(){
        helper('utilidades'); 
        $db = \Config\Database::connect();
        $inicio = restar_meses(date("Y-m-d"),4);
        $data = array();
        for($i=0; $i<5; $i++)
        {
            $a = explode("-",$inicio)[0];
            $m = explode("-",$inicio)[1];
            $ult = cal_days_in_month(CAL_GREGORIAN, $m, $a);
            $ini = "$a-$m-01";
            $fin = "$a-$m-$ult";
    
            $query = $db->query("SELECT COUNT(*)  as total FROM tblorden WHERE deleted_at is NULL AND fecha BETWEEN '$ini' AND '$fin'");
            $datax = $query->getResultArray();
            foreach ($datax as $key => $value) {
                $total = $value["total"];
            }
            
            $data[] = array(
                "total" => $total,  
                "producto" => meses($m), 
            );
            $inicio = sumar_meses($ini,1);
        }
        return $this->genericResponse($data,null,200);
    }
}