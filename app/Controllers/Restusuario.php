<?php namespace App\Controllers;


use App\Models\UsuarioModel;
use MyRestApi;
include_once (dirname(__FILE__) . "/MyRestApi.php");
// 1. Para las solicitudes GET / POST / PUT ordinarias, el encabezado de la solicitud se establece de la siguiente manera:
// Establecer el encabezado de solicitud de formato json
header("Content-type:application/json; charset=utf-8");
// La solicitud entre dominios permite la configuración del nombre de dominio, porque las cookies deben pasarse, no se pueden usar *
header("Access-Control-Allow-Origin: *");
// Solicitud de encabezados permitidos para solicitudes entre dominios
header("Access-Control-Allow-Headers: Content-type");
// Solicitud de consentimiento entre dominios para enviar cookies
header("Access-Control-Allow-Credentials: true");
 
// 2. Solicitud no simple Antes de cada solicitud, se enviará una solicitud de "verificación previa", que es el método de solicitud de opciones. Es principalmente para preguntarle al servidor si permite el acceso de esta solicitud no simple. Si lo permitimos, entonces devolvemos el encabezado de respuesta requerido. El encabezado de solicitud de esta solicitud de verificación previa se establece de la siguiente manera:
// Establecer el encabezado de solicitud de formato json
header("Content-type:application/json; charset=utf-8");
// Configuración de nombre de dominio permitida para solicitud entre dominios
header("Access-Control-Allow-Origin: *");
// Solicitud de encabezados permitidos para solicitudes entre dominios
header("Access-Control-Allow-Headers: Content-type");
header("Vary: Accept-Encoding, Origin");
// Solicitud de consentimiento entre dominios para enviar cookies
header("Access-Control-Allow-Credentials: true");
// métodos permitidos en la solicitud de opciones
header("Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS");
// OPCIONES este tiempo de validez de la solicitud previa, 20 días
header("Access-Control-Max-Age: 1728000");
class Restusuario extends MyRestApi
{

    protected $modelName = 'App\Models\UsuarioModel';
    protected $format = 'json';
    public function index(){
        $usuario = new UsuarioModel();
        $id_sucursal = addslashes($this->request->getGet('id_sucursal'));
        $id_sucursal = $this->desencriptar($id_sucursal);
        return $this->genericResponse($usuario->get(null,$id_sucursal),null,200);
    }

    public function create()
    {
        $usuario = new UsuarioModel();
        $nombre = addslashes($this->request->getPost('nombre'));
        $usuariox = addslashes($this->request->getPost('usuario'));        
        $password = addslashes($this->request->getPost('password'));   
        $password = md5($password);
        $id_empleado = addslashes($this->request->getPost('id_empleado'));     
        $id_sucursal = addslashes($this->request->getPost('id_sucursal'));
        $id_sucursal = $this->desencriptar($id_sucursal);
        if($nombre == "" || $usuariox == ""|| $password == ""|| $id_empleado == ""|| $id_sucursal == ""){
            return $this->genericResponse(null,"Error, tiene que llenar todos los campos.!",500);
        }
        $id = $usuario->insert([
            'nombre' => $nombre,
            'usuario' => $usuariox,
            'password' =>$password,
            'id_empleado' => $id_empleado,
            'id_sucursal' => $id_sucursal,
        ]);
        return $this->genericResponse($this->model->find($id), null, 200);
    }

    public function show($id = null)
    {
        return $this->genericResponse($this->model->find($id),null,200);
    }
    public function actualizar(){
        $usuario = new UsuarioModel();
        
        $nombre = addslashes($this->request->getPost('nombre'));
        $usuariox = addslashes($this->request->getPost('usuario'));        
        $password = addslashes($this->request->getPost('password'));   
        $id_empleado = addslashes($this->request->getPost('id_empleado'));
        $id_usuario = addslashes($this->request->getPost("id_usuario"));
        if(strlen($password) < 30){ 
            $password = md5($password);
        }
        $id_sucursal = addslashes($this->request->getPost('id_sucursal'));
        $id_sucursal = $this->desencriptar($id_sucursal);

        if($nombre == "" || $usuariox == ""|| $password == ""|| $id_empleado == ""|| $id_sucursal == ""){
            return $this->genericResponse(null,"Error, tiene que llenar todos los campos.!",500);
        }
        if (!$usuario->get($id_usuario,$id_sucursal)) {
            return $this->genericResponse(null, array("id_usuario" => "No Existe el Usuario"), 500);
        }
        $usuario->update($id_usuario,[
            'nombre' => $nombre,
            'usuario' => $usuariox,
            'password' =>$password,
            'id_empleado' => $id_empleado,
        ]);
        return $this->genericResponse($this->model->find($id_usuario), null, 200);
    }
    //LA FUNCION PARA VER UN DEPARTAMENTO EN ESPECIFICO YA SE ENCUENTRA REALIZADA Y NO HAY QUE CREAR OTRA

    public function borrar(){
        $usuario = new UsuarioModel();
        $id = addslashes($this->request->getPost('id_usuario'));
        $usuario->delete($id);
        return $this->genericResponse("Usuario eliminado",null,200);
    }
    
    
}