<?php namespace App\Controllers;

use App\Models\MenuModel;
use App\Models\ModuloModel;
use MyRestApi;
include_once (dirname(__FILE__) . "/MyRestApi.php");
header('Access-Control-Allow-Origin: *');
class RestModulo extends MyRestApi
{

    protected $modelName = 'App\Models\ModuloModel';
    protected $format = 'json';
    public function index()
    {
        $menu = new MenuModel();
        $modulo = new ModuloModel();
        
        $id_usuario = addslashes($this->request->getGet('id_usuario'));
        $admin = addslashes($this->request->getGet('admin'));
        $super_admin = addslashes($this->request->getGet('super_admin'));
        $id_usuario = $this->desencriptar($id_usuario);
        $admin = $this->desencriptar($admin);
        $super_admin = $this->desencriptar($super_admin);

        //echo "id_usuario: ".$id_usuario."   -    admin: ".$admin."   -     super_admin: ".$super_admin;

        $array = $menu->get();
        $count = 0;
        $array_nuevo = array();
        foreach ($array as $key => $value) {
            $id_menu = $value['id_menu'];
            $array2= $modulo->get($id_menu, $id_usuario,$admin,$super_admin);
            
            if($array2 != null){
                $array_nuevo[$count]['component'] = 'CNavGroup';
                $array_nuevo[$count]['name'] = $value['nombre'];
                $array_nuevo[$count]['to'] = "/".strtolower($value['nombre']);
                $array_nuevo[$count]['icon'] = $value['icono'];
                foreach ($array2 as $key1 => $value1) {
                    $array2[$key1]['component'] = 'CNavItem';
                    $array2[$key1]['name'] = $value1['nombre'];
                    $array2[$key1]['to'] = "/".strtolower($value1['filename']);
                }
                $array_nuevo[$count]['items'] = $array2;
                $count++;
            }
        }
        return $this->genericResponse($array_nuevo,null,200);
    }
}