<?php namespace App\Controllers;


use App\Models\SucursalModel;
use MyRestApi;
include_once (dirname(__FILE__) . "/MyRestApi.php");
// 1. Para las solicitudes GET / POST / PUT ordinarias, el encabezado de la solicitud se establece de la siguiente manera:
// Establecer el encabezado de solicitud de formato json
header("Content-type:application/json; charset=utf-8");
// La solicitud entre dominios permite la configuración del nombre de dominio, porque las cookies deben pasarse, no se pueden usar *
header("Access-Control-Allow-Origin: *");
// Solicitud de encabezados permitidos para solicitudes entre dominios
header("Access-Control-Allow-Headers: Content-type");
// Solicitud de consentimiento entre dominios para enviar cookies
header("Access-Control-Allow-Credentials: true");
 
// 2. Solicitud no simple Antes de cada solicitud, se enviará una solicitud de "verificación previa", que es el método de solicitud de opciones. Es principalmente para preguntarle al servidor si permite el acceso de esta solicitud no simple. Si lo permitimos, entonces devolvemos el encabezado de respuesta requerido. El encabezado de solicitud de esta solicitud de verificación previa se establece de la siguiente manera:
// Establecer el encabezado de solicitud de formato json
header("Content-type:application/json; charset=utf-8");
// Configuración de nombre de dominio permitida para solicitud entre dominios
header("Access-Control-Allow-Origin: *");
// Solicitud de encabezados permitidos para solicitudes entre dominios
header("Access-Control-Allow-Headers: Content-type");
header("Vary: Accept-Encoding, Origin");
// Solicitud de consentimiento entre dominios para enviar cookies
header("Access-Control-Allow-Credentials: true");
// métodos permitidos en la solicitud de opciones
header("Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS");
// OPCIONES este tiempo de validez de la solicitud previa, 20 días
header("Access-Control-Max-Age: 1728000");
class Restsucursal extends MyRestApi
{

    protected $modelName = 'App\Models\SucursalModel';
    protected $format = 'json';
    public function index(){
        $sucursal = new SucursalModel();
        return $this->genericResponse($sucursal->get(),null,200);
    }

    public function create()
    {
        $sucursal = new SucursalModel();
        $nombre = addslashes($this->request->getPost('nombre'));
        $descripcion = addslashes($this->request->getPost('descripcion'));        
        if($nombre == ""){
            return $this->genericResponse(null,"Error, tiene que llenar todos los campos.!",500);
        }
        $id = $sucursal->insert([
            'descripcion' => $descripcion,
            'nombre' => $nombre,
        ]);
        return $this->genericResponse($this->model->find($id), null, 200);
    }

    public function show($id = null)
    {
        $id = $this->desencriptar($id);  
        return $this->genericResponse($this->model->find($id),null,200);
    }
    public function actualizar(){
        $sucursal = new SucursalModel();
        $id = addslashes($this->request->getPost('id_sucursal'));
        $id = $this->desencriptar($id); 

        $nombre = addslashes($this->request->getPost('nombre'));
        $propietario = addslashes($this->request->getPost('propietario'));   
        $direccion = addslashes($this->request->getPost('direccion'));
        $telefono1 = addslashes($this->request->getPost('telefono1'));  
        $telefono2 = addslashes($this->request->getPost('telefono2'));
        $email = addslashes($this->request->getPost('email'));  
        $moneda = addslashes($this->request->getPost('moneda'));
        $simbolo = addslashes($this->request->getPost('simbolo'));  

        if($nombre == "" || $propietario == "" || $direccion == "" || $telefono1 == "" || $telefono2 == "" || $email == "" || $moneda == "" || $simbolo == ""){
            return $this->genericResponse(null,"Error, tiene que llenar todos los campos.!",500);
        }
        if (!$sucursal->get($id)) {
            return $this->genericResponse(null, array("id_sucursal" => "No Existe la Sucursal!!"), 500);
        }
        $sucursal->update($id,[
            'nombre' => $nombre,
            'propietario' => $propietario,
            'direccion' => $direccion,
            'telefono1' => $telefono1,
            'telefono2' => $telefono2,
            'email' => $email,
            'moneda' => $moneda,
            'simbolo' => $simbolo,
        ]);
        return $this->genericResponse($this->model->find($id), null, 200);
    }
    //LA FUNCION PARA VER UN DEPARTAMENTO EN ESPECIFICO YA SE ENCUENTRA REALIZADA Y NO HAY QUE CREAR OTRA

    public function borrar(){
        $sucursal = new SucursalModel();
        $id = addslashes($this->request->getPost('id_depto'));
        $sucursal->delete($id);
        return $this->genericResponse("Departamento eliminado",null,200);
    }

    public function publico(){
        $sucursal = new SucursalModel();
        $array = $sucursal->get();
        $nuevo_array = array();
        $count = 0;
        foreach ($array as $key => $value) {
            $nuevo_array[] = array(
                'text' => $value['nombre'],
                'value' => $value['id_sucursal']
            );
            $count++;
        }
        return $this->genericResponse($nuevo_array,null,200);
    }
}