<?php

use CodeIgniter\RESTful\ResourceController;

class MyRestApi extends ResourceController
{

    public function genericPaginateResponse($data, $msj, $code, $pager)
    {
        $pageURI = $pager->getPageURI();
        if ($code == 200 && count($data) > 0) {
            return $this->respond(array(
                "data" => array(
                    "data" => $data,
                    "current_page" => $pager->getCurrentPage(),
                    "last_page" => $pager->getLastPage(),
                    "last_page_url" => $pageURI."?page=".$pager->getLastPage(),
                    "first_page" => $pager->getFirstPage(),
                    "first_page_url" => $pageURI."?page=".$pager->getFirstPage(),
                    "prev_page_url" => $pager->getPreviousPageURI(),
                    "next_page_url" => $pager->getNextPageURI(),
                    "path" => $pager->getPageURI(),
                    "per_page" => $pager->getPerPage(),
                    "more" => $pager->hasMore()
                ),
                "code" => $code
            )); //, 404, "No hay nada"
        } else {
            return $this->respond(array(
                "msj" => $msj,
                "code" => $code
            ));
        }
    }

    public function genericResponse($data, $msj, $code)
    {
        if ($code == 200) {
            return $this->respond(array(
                "data" => $data,
                "code" => $code
            )); //, 404, "No hay nada"
        } else{
            return $this->respond(array(
                "msj" => $msj,
                "code" => $code
            ));
            
        }

    }
    public function encriptar($id){
        if($id == 0){
            $numero = 0;
            //sumar al id el numero no capicua 134430
            $numero = $numero + 134430;
        }
        else{
            //Elevar el id a su cuadrado
            $numero = $id * $id;
            //sumar al id el numero capicua 134431
            $numero = $numero + 134431;
        }
       
        if($numero % 2 == 0){
            $numero = $numero /2;
        }
        //Si el numero es impar se le suma 1 y se divide entre 2
        else{
            $numero = $numero +1;
            $numero = $numero /2;
        }
        //Se vuelve a multiplicar el numero por un numero capicua mas pequenio
        $numero = $numero *272;
        $cadena = "";
        $numero_cadena = (string)$numero;
        for ($i=0; $i < strlen($numero); $i++) { 
            $caracteres_permitidos = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890';
            $codigo_generado = substr(str_shuffle($caracteres_permitidos), 0, 4);
            $cadena.=$codigo_generado.$numero_cadena[$i];
        }
        return $cadena;
    }
    public function desencriptar($cadena){
        $numero_cadena = "";
        for ($i=0; $i < strlen($cadena) ; $i++) { 
            $x = $i+1;
            if(($x % 5) == 0){
                $numero_cadena.=$cadena[$i];
            }
        }
        $numero = intval($numero_cadena);
        $numero = $numero /272;
        $numero = $numero * 2;
        $numero = $numero - 134431;
        if($numero == -1){
            return 0;
        }
        else{
            if(fmod(sqrt($numero),1) !== 0.0){
                $numero--;
                return sqrt($numero);
            }
            else{
                return sqrt($numero);
            }
        }
        
    }

}