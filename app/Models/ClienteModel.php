<?php
    namespace App\Models;

    use CodeIgniter\Model;


    class ClienteModel extends Model
    {
        protected $table = 'tblcliente';
        protected $primaryKey = 'id_cliente';
        protected $allowedFields = ['nombres', 'apellidos','direccion','telefono','dui'];
        protected $useSoftDeletes = true;
        protected $useTimestamps = false;
        protected $createdField  = 'created_at';
        protected $updatedField  = 'updated_at';
        protected $deletedField  = 'deleted_at';

        function getAll(){
            return $this->asArray()
            ->select('tblmodulo.*, tblmenu.nombre as nombre_menu')
            ->join('tblmenu','tblmenu.id_menu = tblmodulo.id_meno')
            ->first();
        }

        public function get($id_cliente = null){
            
            if($id_cliente == null){
                return $this->findAll();
            }
            return $this->asArray()
                ->where('tblcliente.id_cliente',$id_cliente)->findAll();
        }

        public function verificar_usuario($id_usuario = null){
            $data = $this->db->query("SELECT * FROM tblusuario WHERE id_usuario = '$id_usuario'");
            return $data;
        }
    }


?>