<?php
    namespace App\Models;

    use CodeIgniter\Model;


    class MenuModel extends Model
    {
        protected $table = 'tblmenu';
        protected $primary_key = 'id_menu';
        protected $allowedFields = ['nombre','prioridad','icono','visible','super_admin'];
        protected $useSoftDeletes = true;
        protected $useTimestamps = false;
        protected $createdField  = 'created_at';
        protected $updatedField  = 'updated_at';
        protected $deletedField  = 'deleted_at';

        public function get($id_menu = null){
            if($id_menu == null){
                return $this->findAll();
            }
            return $this->asArray()
            ->where(['id_menu' => $id_menu])
            ->first();
        }
    }


?>