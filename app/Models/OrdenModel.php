<?php
    namespace App\Models;

    use CodeIgniter\Model;


    class OrdenModel extends Model
    {
        protected $table = 'tblorden';
        protected $primaryKey = 'id_orden';
        protected $allowedFields = ['numero_seguimiento', 'id_cliente','id_envio','id_recibo','fecha','hora','detalles','correo','id_estado', 'dui_recibe','nombre_recibe'];
        protected $useSoftDeletes = true;
        protected $useTimestamps = false;
        protected $createdField  = 'created_at';
        protected $updatedField  = 'updated_at';
        protected $deletedField  = 'deleted_at';

        function getAll(){
            return $this->asArray()
            ->select('tblmodulo.*, tblmenu.nombre as nombre_menu')
            ->join('tblmenu','tblmenu.id_menu = tblmodulo.id_meno')
            ->first();
        }

        public function get($id_orden = null){
            
            if($id_orden == null){
                return $this->asArray()
                ->select("tblorden.id_orden, tblorden.id_estado, tblorden.nombre_recibe, tblorden.dui_recibe, tblorden.numero_seguimiento, tblorden.fecha, tblorden.hora, tblorden.detalles, tblorden.correo, sucursal1.nombre as 'sucursal_envio', sucursal2.nombre as 'recibo', CONCAT(tblcliente.nombres, ' ', tblcliente.apellidos) as 'nombre_cliente', tblestado_orden.nombre as 'nombre_estado' ")
                ->join('tblcliente','tblcliente.id_cliente = tblorden.id_cliente')
                ->join('tblsucursal as sucursal1','sucursal1.id_sucursal = tblorden.id_envio')
                ->join('tblsucursal as sucursal2','sucursal2.id_sucursal = tblorden.id_recibo')
                ->join('tblestado_orden','tblestado_orden.id_estado_orden = tblorden.id_estado')
                ->findAll();
            }

            return $this->asArray()
                ->select("tblorden.id_orden, tblorden.id_estado, tblorden.nombre_recibe, tblorden.dui_recibe, tblorden.numero_seguimiento, tblorden.fecha, tblorden.hora, tblorden.detalles, tblorden.correo, sucursal1.nombre as 'sucursal_envio', sucursal2.nombre as 'recibo', CONCAT(tblcliente.nombres, ' ', tblcliente.apellidos) as 'nombre_cliente', tblestado_orden.nombre as 'nombre_estado'")
                ->join('tblcliente','tblcliente.id_cliente = tblorden.id_cliente')
                ->join('tblsucursal as sucursal1','sucursal1.id_sucursal = tblorden.id_envio')
                ->join('tblsucursal as sucursal2','sucursal2.id_sucursal = tblorden.id_recibo')
                ->join('tblestado_orden','tblestado_orden.id_estado_orden = tblorden.id_estado')
                ->where('tblorden.id_orden',$id_orden)->findAll();
        }
        public function estado($id_estado = null){
            if($id_estado == null){
                return $this->asArray()
                ->select("tblorden.id_orden, tblorden.id_estado, tblorden.nombre_recibe, tblorden.dui_recibe, tblorden.numero_seguimiento, tblorden.fecha, tblorden.hora, tblorden.detalles, tblorden.correo, sucursal1.nombre as 'sucursal_envio', sucursal2.nombre as 'recibo', CONCAT(tblcliente.nombres, ' ', tblcliente.apellidos) as 'nombre_cliente', tblestado_orden.nombre as 'nombre_estado' ")
                ->join('tblcliente','tblcliente.id_cliente = tblorden.id_cliente')
                ->join('tblsucursal as sucursal1','sucursal1.id_sucursal = tblorden.id_envio')
                ->join('tblsucursal as sucursal2','sucursal2.id_sucursal = tblorden.id_recibo')
                ->join('tblestado_orden','tblestado_orden.id_estado_orden = tblorden.id_estado')
                ->findAll();
            }

            return $this->asArray()
                ->select("tblorden.id_orden, tblorden.id_estado, tblorden.nombre_recibe, tblorden.dui_recibe, tblorden.numero_seguimiento, tblorden.fecha, tblorden.hora, tblorden.detalles, tblorden.correo, sucursal1.nombre as 'sucursal_envio', sucursal2.nombre as 'recibo', CONCAT(tblcliente.nombres, ' ', tblcliente.apellidos) as 'nombre_cliente', tblestado_orden.nombre as 'nombre_estado'")
                ->join('tblcliente','tblcliente.id_cliente = tblorden.id_cliente')
                ->join('tblsucursal as sucursal1','sucursal1.id_sucursal = tblorden.id_envio')
                ->join('tblsucursal as sucursal2','sucursal2.id_sucursal = tblorden.id_recibo')
                ->join('tblestado_orden','tblestado_orden.id_estado_orden = tblorden.id_estado')
                ->where('tblorden.id_estado',$id_estado)->findAll();
        }
        public function verificar_usuario($id_usuario = null){
            $data = $this->db->query("SELECT * FROM tblusuario WHERE id_usuario = '$id_usuario'");
            return $data;
        }
        public function actualizar_paquetes($id_cliente, $id_orden){
            $db = \Config\Database::connect();
            $builder = $db->table('tblpaquete');
            $data = [
                'id_orden' => $id_orden,
            ];
            $builder->where('id_cliente', $id_cliente);
            $builder->where('id_orden', '0');
            $builder->update($data);
            return $builder;
        }
        public function recuperar_paquetes($id_orden){
            $db = \Config\Database::connect();
            $builder = $db->table('tblpaquete');
            $data = [
                'id_orden' => '0',
            ];
            $builder->where('id_orden', $id_orden);
            $builder->update($data);
            return $builder;
        }
    }


?>