<?php
    namespace App\Models;

    use CodeIgniter\Model;


    class SucursalModel extends Model
    {
        protected $table = 'tblsucursal';
        protected $primaryKey = 'id_sucursal';
        protected $allowedFields = ['nombre', 'propietario','direccion','telefono1','telefono2','email','moneda','simbolo'];
        protected $useSoftDeletes = true;
        protected $useTimestamps = false;
        protected $createdField  = 'created_at';
        protected $updatedField  = 'updated_at';
        protected $deletedField  = 'deleted_at';

        function getAll(){
            return $this->asArray()
            ->select('tblmodulo.*, tblmenu.nombre as nombre_menu')
            ->join('tblmenu','tblmenu.id_menu = tblmodulo.id_meno')
            ->first();
        }

        public function get($id_sucursal = null){
            
            if($id_sucursal == null){
                return $this->findAll();
            }
            return $this->asArray()
                ->where('tblsucursal.id_sucursal',$id_sucursal)->findAll();
        }

        public function verificar_usuario($id_usuario = null){
            $data = $this->db->query("SELECT * FROM tblusuario WHERE id_usuario = '$id_usuario'");
            return $data;
        }
    }


?>