<?php
    namespace App\Models;

    use CodeIgniter\Model;


    class ServicioModel extends Model
    {
        protected $table = 'tblservicio';
        protected $primaryKey = 'id_servicio';
        protected $allowedFields = ['nombre', 'descripcion'];
        protected $useSoftDeletes = true;
        protected $useTimestamps = false;
        protected $createdField  = 'created_at';
        protected $updatedField  = 'updated_at';
        protected $deletedField  = 'deleted_at';

        function getAll(){
            return $this->asArray()
            ->select('tblmodulo.*, tblmenu.nombre as nombre_menu')
            ->join('tblmenu','tblmenu.id_menu = tblmodulo.id_meno')
            ->first();
        }

        public function get($id_servicio = null){
            
            if($id_servicio == null){
                return $this->findAll();
            }
            return $this->asArray()
                ->where('tblservicio.id_servicio',$id_servicio)->findAll();
        }

        public function verificar_usuario($id_usuario = null){
            $data = $this->db->query("SELECT * FROM tblusuario WHERE id_usuario = '$id_usuario'");
            return $data;
        }
    }


?>