<?php
    namespace App\Models;

    use CodeIgniter\Model;


    class DashboardModel extends Model
    {
        function getAll(){
            return $this->asArray()
            ->select('tblmodulo.*, tblmenu.nombre as nombre_menu')
            ->join('tblmenu','tblmenu.id_menu = tblmodulo.id_meno')
            ->first();
        }

        public function get($id_sucursal = null){
            
            if($id_sucursal == null){
                return $this->findAll();
            }
            return $this->asArray()
                ->where('tblsucursal.id_sucursal',$id_sucursal)->findAll();
        }

        public function verificar_usuario($id_usuario = null){
            $data = $this->db->query("SELECT * FROM tblusuario WHERE id_usuario = '$id_usuario'");
            return $data;
        }

        public function informacion_cards()
        {
            $data = $this->db->query("SELECT (SELECT COUNT(*) FROM tblusuario WHERE tblusuario.deleted_at is NULL) as 'total_usuarios', (SELECT COUNT(*) FROM tblpaquete WHERE tblpaquete.deleted_at is NULL) as 'total_paquetes',(SELECT COUNT(*) FROM tblorden WHERE tblorden.deleted_at is NULL) as 'total_ordenes',(SELECT COUNT(*) FROM tblcliente WHERE tblcliente.deleted_at is NULL) as 'total_clientes' ");
            return $data;
        }
    }


?>