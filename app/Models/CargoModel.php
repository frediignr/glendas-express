<?php
    namespace App\Models;

    use CodeIgniter\Model;


    class CargoModel extends Model
    {
        protected $table = 'tblcargo';
        protected $primaryKey = 'id_cargo';
        protected $allowedFields = ['nombre', 'descripcion','id_depto'];
        protected $useSoftDeletes = true;
        protected $useTimestamps = false;
        protected $createdField  = 'created_at';
        protected $updatedField  = 'updated_at';
        protected $deletedField  = 'deleted_at';

        function getAll(){
            return $this->asArray()
            ->select('tblmodulo.*, tblmenu.nombre as nombre_menu')
            ->join('tblmenu','tblmenu.id_menu = tblmodulo.id_meno')
            ->first();
        }

        public function get($id_cargo = null){
            
            if($id_cargo == null){
                return $this->asArray()
                ->select("tblcargo.id_cargo, tblcargo.nombre, tblcargo.descripcion, tbldepto.nombre as 'nombre_depto'")
                ->join('tbldepto','tbldepto.id_depto = tblcargo.id_depto')->findAll();
            }

            return $this->asArray()
            ->select("tblcargo.id_cargo, tblcargo.nombre, tblcargo.descripcion, tbldepto.nombre as 'nombre_depto'")
            ->join('tbldepto','tbldepto.id_depto = tblcargo.id_depto')
            ->where('tblcargo.id_cargo',$id_cargo)->findAll();

               
        }

        public function verificar_usuario($id_usuario = null){
            $data = $this->db->query("SELECT * FROM tblusuario WHERE id_usuario = '$id_usuario'");
            return $data;
        }
    }


?>