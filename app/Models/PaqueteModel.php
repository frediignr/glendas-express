<?php
    namespace App\Models;

    use CodeIgniter\Model;


    class PaqueteModel extends Model
    {
        protected $table = 'tblpaquete';
        protected $primaryKey = 'id_paquete';
        protected $allowedFields = ['descripcion', 'fecha','hora','peso','id_cliente','id_tipo_paquete','precio','lugar_compra'];
        protected $useSoftDeletes = true;
        protected $useTimestamps = false;
        protected $createdField  = 'created_at';
        protected $updatedField  = 'updated_at';
        protected $deletedField  = 'deleted_at';

        function getAll(){
            return $this->asArray()
            ->select('tblmodulo.*, tblmenu.nombre as nombre_menu')
            ->join('tblmenu','tblmenu.id_menu = tblmodulo.id_meno')
            ->first();
        }

        public function get($id_paquete = null){
            
            if($id_paquete == null){
                return $this->asArray()
                ->select("tblpaquete.id_paquete, tblpaquete.precio, tblpaquete.lugar_compra, tblpaquete.descripcion, tblpaquete.fecha, tblpaquete.hora, tblpaquete.peso, tblcliente.nombres, tblcliente.apellidos, tbltipo_paquete.nombre as 'tipo_paquete'")
                ->join('tblcliente','tblcliente.id_cliente = tblpaquete.id_cliente')
                ->join('tbltipo_paquete','tbltipo_paquete.id_tipo_paquete = tblpaquete.id_tipo_paquete')->findAll();
            }

            return $this->asArray()
            ->select("tblpaquete.id_paquete, tblpaquete.precio, tblpaquete.lugar_compra, tblpaquete.descripcion, tblpaquete.fecha, tblpaquete.hora, tblpaquete.peso, tblcliente.nombres, tblcliente.apellidos, tbltipo_paquete.nombre as 'tipo_paquete'")
            ->join('tblcliente','tblcliente.id_cliente = tblpaquete.id_cliente')
            ->join('tbltipo_paquete','tbltipo_paquete.id_tipo_paquete = tblpaquete.id_tipo_paquete')
            ->where('tblpaquete.id_paquete',$id_paquete)->findAll();

               
        }
        public function cliente($id_cliente = null){
            
            if($id_cliente == null){
                return $this->asArray()
                ->select("tblpaquete.id_paquete, tblpaquete.precio, tblpaquete.lugar_compra, tblpaquete.descripcion, tblpaquete.fecha, tblpaquete.hora, tblpaquete.peso, tblcliente.nombres, tblcliente.apellidos, tbltipo_paquete.nombre as 'tipo_paquete'")
                ->join('tblcliente','tblcliente.id_cliente = tblpaquete.id_cliente')
                ->join('tbltipo_paquete','tbltipo_paquete.id_tipo_paquete = tblpaquete.id_tipo_paquete')->findAll();
            }

            return $this->asArray()
            ->select("tblpaquete.id_paquete, tblpaquete.precio, tblpaquete.lugar_compra, tblpaquete.descripcion, tblpaquete.fecha, tblpaquete.hora, tblpaquete.peso, tblcliente.nombres, tblcliente.apellidos, tbltipo_paquete.nombre as 'tipo_paquete'")
            ->join('tblcliente','tblcliente.id_cliente = tblpaquete.id_cliente')
            ->join('tbltipo_paquete','tbltipo_paquete.id_tipo_paquete = tblpaquete.id_tipo_paquete')
            ->where('tblpaquete.id_cliente',$id_cliente)
            ->where('tblpaquete.id_orden',"0")
            ->findAll();

               
        }
        public function orden($id_orden = null){
            
            if($id_orden == null){
                return $this->asArray()
                ->select("tblpaquete.id_paquete, tblpaquete.precio, tblpaquete.lugar_compra, tblpaquete.descripcion, tblpaquete.fecha, tblpaquete.hora, tblpaquete.peso, tblcliente.nombres, tblcliente.apellidos, tbltipo_paquete.nombre as 'tipo_paquete'")
                ->join('tblcliente','tblcliente.id_cliente = tblpaquete.id_cliente')
                ->join('tbltipo_paquete','tbltipo_paquete.id_tipo_paquete = tblpaquete.id_tipo_paquete')->findAll();
            }

            return $this->asArray()
            ->select("tblpaquete.id_paquete, tblpaquete.precio, tblpaquete.lugar_compra, tblpaquete.descripcion, tblpaquete.fecha, tblpaquete.hora, tblpaquete.peso, tblcliente.nombres, tblcliente.apellidos, tbltipo_paquete.nombre as 'tipo_paquete'")
            ->join('tblcliente','tblcliente.id_cliente = tblpaquete.id_cliente')
            ->join('tbltipo_paquete','tbltipo_paquete.id_tipo_paquete = tblpaquete.id_tipo_paquete')
            ->where('tblpaquete.id_orden',$id_orden)
            ->findAll();

               
        }
        public function verificar_usuario($id_usuario = null){
            $data = $this->db->query("SELECT * FROM tblusuario WHERE id_usuario = '$id_usuario'");
            return $data;
        }
    }


?>