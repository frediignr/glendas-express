<?php
    namespace App\Models;

    use CodeIgniter\Model;


    class FacturaModel extends Model
    {
        protected $table = 'tblfactura';
        protected $primaryKey = 'id_factura';
        protected $allowedFields = ['id_servicio', 'id_orden','total','fecha','hora'];
        protected $useSoftDeletes = true;
        protected $useTimestamps = false;
        protected $createdField  = 'created_at';
        protected $updatedField  = 'updated_at';
        protected $deletedField  = 'deleted_at';

        function getAll(){
            return $this->asArray()
            ->select('tblmodulo.*, tblmenu.nombre as nombre_menu')
            ->join('tblmenu','tblmenu.id_menu = tblmodulo.id_meno')
            ->first();
        }

        public function get($id_factura = null){
            if($id_factura == null){
                return $this->asArray()
                ->select("tblfactura.id_factura, tblfactura.id_servicio, tblfactura.id_orden, tblfactura.total, tblfactura.fecha, tblfactura.hora, tblservicio.nombre as 'nombre_servicio', tblorden.numero_seguimiento, CONCAT(tblcliente.nombres, ' ', tblcliente.apellidos) as 'cliente'")
                ->join('tblservicio','tblservicio.id_servicio = tblfactura.id_servicio')
                ->join('tblorden','tblorden.id_orden = tblfactura.id_orden')
                ->join('tblcliente','tblcliente.id_cliente = tblorden.id_cliente')
                ->findAll();
            }
            return $this->asArray()
            ->select("tblfactura.id_factura, tblfactura.id_servicio, tblfactura.id_orden, tblfactura.total, tblfactura.fecha, tblfactura.hora, tblservicio.nombre as 'nombre_servicio', tblorden.numero_seguimiento, CONCAT(tblcliente.nombres, ' ', tblcliente.apellidos) as 'cliente'")
            ->join('tblservicio','tblservicio.id_servicio = tblfactura.id_servicio')
            ->join('tblorden','tblorden.id_orden = tblfactura.id_orden')
            ->join('tblcliente','tblcliente.id_cliente = tblorden.id_cliente')
            ->where('tblfactura.id_factura',$id_factura)->findAll();    
        }
        public function verificar_usuario($id_usuario = null){
            $data = $this->db->query("SELECT * FROM tblusuario WHERE id_usuario = '$id_usuario'");
            return $data;
        }
    }


?>