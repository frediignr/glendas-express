<?php
    namespace App\Models;

    use CodeIgniter\Model;


    class TipoPaquete extends Model
    {
        protected $table = 'tbltipo_paquete';
        protected $primaryKey = 'id_tipo_paquete';
        protected $allowedFields = ['nombre', 'descripcion'];
        protected $useSoftDeletes = true;
        protected $useTimestamps = false;
        protected $createdField  = 'created_at';
        protected $updatedField  = 'updated_at';
        protected $deletedField  = 'deleted_at';

        function getAll(){
            return $this->asArray()
            ->select('tblmodulo.*, tblmenu.nombre as nombre_menu')
            ->join('tblmenu','tblmenu.id_menu = tblmodulo.id_meno')
            ->first();
        }

        public function get($id_tipo_paquete = null){
            
            if($id_tipo_paquete == null){
                return $this->findAll();
            }
            return $this->asArray()
                ->where('tbltipo_paquete.id_tipo_paquete',$id_tipo_paquete)->findAll();
        }

        public function verificar_usuario($id_usuario = null){
            $data = $this->db->query("SELECT * FROM tblusuario WHERE id_usuario = '$id_usuario'");
            return $data;
        }
    }


?>