<?php
    namespace App\Models;

    use CodeIgniter\Model;


    class LoginModel extends Model
    {
        protected $table = 'tblusuario';
        protected $primaryKey = 'id_usuario';
        protected $allowedFields = ['nombre', 'usuario','imagen','admin','id_empleado','id_sucursal','super_admin'];
        protected $useSoftDeletes = true;
        protected $useTimestamps = false;
        protected $createdField  = 'created_at';
        protected $updatedField  = 'updated_at';
        protected $deletedField  = 'deleted_at';

        
        public function get($id_usuario = null){
            if($id_usuario == null){
                return $this->findAll();
            }
            return $this->where('id_usuario',$id_usuario)->findAll();
        }
    }


?>