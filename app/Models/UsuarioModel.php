<?php
    namespace App\Models;

    use CodeIgniter\Model;


    class UsuarioModel extends Model
    {
        protected $table = 'tblusuario';
        protected $primaryKey = 'id_usuario';
        protected $allowedFields = ['nombre', 'usuario','password','imagen','admin','id_empleado','id_sucursal'];
        protected $useSoftDeletes = true;
        protected $useTimestamps = false;
        protected $createdField  = 'created_at';
        protected $updatedField  = 'updated_at';
        protected $deletedField  = 'deleted_at';

        function getAll(){
            return $this->asArray()
            ->select('tblmodulo.*, tblmenu.nombre as nombre_menu')
            ->join('tblmenu','tblmenu.id_menu = tblmodulo.id_meno')
            ->first();
        }

        public function get($id_usuario = null, $id_sucursal = null){
            
            if($id_usuario == null){
                return $this->asArray()
                ->select("tblusuario.id_usuario, tblusuario.nombre, tblusuario.usuario, tblempleado.nombres, tblempleado.apellidos")
                ->join('tblempleado','tblempleado.id_empleado = tblusuario.id_empleado')
                ->where('tblusuario.id_sucursal',$id_sucursal)->findAll();
            }

            return $this->asArray()
            ->select("tblusuario.id_usuario, tblusuario.nombre, tblusuario.usuario, tblempleado.nombres, tblempleado.apellidos")
            ->join('tblempleado','tblempleado.id_empleado = tblusuario.id_empleado')
            ->where('tblusuario.id_usuario',$id_usuario)
            ->where('tblusuario.id_sucursal',$id_sucursal)
            ->findAll();   
        }

        public function verificar_usuario($id_usuario = null){
            $data = $this->db->query("SELECT * FROM tblusuario WHERE id_usuario = '$id_usuario'");
            return $data;
        }
    }


?>