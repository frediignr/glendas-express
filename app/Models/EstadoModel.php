<?php
    namespace App\Models;

    use CodeIgniter\Model;


    class EstadoModel extends Model
    {
        protected $table = 'tblestado_orden';
        protected $primaryKey = 'id_estado_orden';
        protected $allowedFields = ['nombre'];
        protected $useSoftDeletes = true;
        protected $useTimestamps = false;
        protected $createdField  = 'created_at';
        protected $updatedField  = 'updated_at';
        protected $deletedField  = 'deleted_at';

        function getAll(){
            return $this->asArray()
            ->select('tblmodulo.*, tblmenu.nombre as nombre_menu')
            ->join('tblmenu','tblmenu.id_menu = tblmodulo.id_meno')
            ->first();
        }

        public function get($id_estado_orden = null){
            
            if($id_estado_orden == null){
                return $this->findAll();
            }
            return $this->asArray()
                ->where('tblestado_orden.id_estado_orden',$id_estado_orden)->findAll();
        }

        public function verificar_usuario($id_usuario = null){
            $data = $this->db->query("SELECT * FROM tblusuario WHERE id_usuario = '$id_usuario'");
            return $data;
        }
    }


?>