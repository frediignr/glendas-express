<?php
    namespace App\Models;

    use CodeIgniter\Model;


    class ModuloModel extends Model
    {
        protected $table = 'tblmodulo';
        protected $primaryKey = 'id_modulo';
        protected $allowedFields = ['nombre', 'description','filename','visible','id_menu'];
        protected $useSoftDeletes = true;
        protected $useTimestamps = false;
        protected $createdField  = 'created_at';
        protected $updatedField  = 'updated_at';
        protected $deletedField  = 'deleted_at';

        function getAll(){
            return $this->asArray()
            ->select('tblmodulo.*, tblmenu.nombre as nombre_menu')
            ->join('tblmenu','tblmenu.id_menu = tblmodulo.id_meno')
            ->first();
        }

        public function get($id_menu = null,$id_usuario = null, $admin = null, $super_admin = null){
            if($super_admin == 1 && $id_menu == 1){
               
                return $this->asArray()
                ->select('tblmodulo.id_modulo, tblmodulo.nombre, tblmodulo.filename')
                ->where('tblmodulo.id_menu',$id_menu)
                ->where('tblmodulo.visible',1)->findAll();
            }
            if($admin == 1 && $id_menu != 1){
                //echo $id_menu;
                return $this->asArray()
                ->select('tblmodulo.id_modulo, tblmodulo.nombre, tblmodulo.filename')
                ->where('tblmodulo.id_menu',$id_menu)
                ->where('tblmodulo.visible',1)->findAll();
            }
            if($admin == 0 && $super_admin == 0){
                return $this->asArray()
                ->select('tblmodulo.id_modulo, tblmodulo.nombre, tblmodulo.filename')
                ->join('tblusuario_modulo','tblusuario_modulo.id_modulo = tblmodulo.id_modulo')
                ->join('tblusuario','tblusuario.id_usuario = tblusuario_modulo.id_usuario')
                ->where('tblusuario.id_usuario',$id_usuario)
                ->where('tblmodulo.deleted_at',null)
                ->where('tblmodulo.visible',1)
                ->findAll();
            }
            if($id_menu == null){
                return $this->findAll();
            }
        }
    }


?>