<?php
    namespace App\Models;

    use CodeIgniter\Model;


    class EmpleadoModel extends Model
    {
        protected $table = 'tblempleado';
        protected $primaryKey = 'id_empleado';
        protected $allowedFields = ['nombres', 'apellidos','direccion','telefono','fecha_nacimiento','fecha_inicio','id_cargo','id_sucursal'];
        protected $useSoftDeletes = true;
        protected $useTimestamps = false;
        protected $createdField  = 'created_at';
        protected $updatedField  = 'updated_at';
        protected $deletedField  = 'deleted_at';

        function getAll(){
            return $this->asArray()
            ->select('tblmodulo.*, tblmenu.nombre as nombre_menu')
            ->join('tblmenu','tblmenu.id_menu = tblmodulo.id_meno')
            ->first();
        }

        public function get($id_empleado = null, $id_sucursal = null){
            if($id_empleado == null && $id_sucursal == null){
                return $this->asArray()
                ->select("tblempleado.id_empleado, tblempleado.nombres, tblempleado.apellidos, tblempleado.telefono, tblempleado.direccion, tblcargo.nombre as 'nombre_cargo'")
                ->join('tblcargo','tblcargo.id_cargo = tblempleado.id_cargo')->findAll();
            }
            if($id_empleado == null){
                return $this->asArray()
                ->select("tblempleado.id_empleado, tblempleado.nombres, tblempleado.apellidos, tblempleado.telefono, tblempleado.direccion, tblcargo.nombre as 'nombre_cargo'")
                ->join('tblcargo','tblcargo.id_cargo = tblempleado.id_cargo')
                ->where('tblempleado.id_sucursal',$id_sucursal)->findAll();
            }

            return $this->asArray()
            ->select("tblempleado.id_empleado, tblempleado.nombres, tblempleado.apellidos, tblempleado.telefono, tblempleado.direccion, tblcargo.nombre as 'nombre_cargo'")
            ->join('tblcargo','tblcargo.id_cargo = tblempleado.id_cargo')
            ->where('tblempleado.id_empleado',$id_empleado)
            ->where('tblempleado.id_sucursal',$id_sucursal)
            ->findAll();   
        }

        public function verificar_usuario($id_usuario = null){
            $data = $this->db->query("SELECT * FROM tblusuario WHERE id_usuario = '$id_usuario'");
            return $data;
        }
    }


?>